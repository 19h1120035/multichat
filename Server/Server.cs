﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class Server : Form
    {
        public Server()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;

            Connect();
        }

        /// <summary>
        /// Gửi tin cho tất cả client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e)
        {
            foreach (Socket item in clientList)
            {
                Send(item);
            }
            AddMessage(txtMessage.Text);
            txtMessage.Clear();
        }


        /*
         * Cần:
         * Socket
         * IP
         */

        IPEndPoint IP;
        Socket server;
        List<Socket> clientList;

        /// <summary>
        /// Kết nối tới server
        /// </summary>
        void Connect()
        {
            clientList = new List<Socket>();

            // IP: địa chỉ của server
            IP = new IPEndPoint(IPAddress.Any, 9999); //9999: port
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            server.Bind(IP);

            Thread listen = new Thread(() =>
            {
                try
                {
                    while (true)
                    {
                        server.Listen(100);
                        Socket client = server.Accept();
                        clientList.Add(client);
                        Thread receive = new Thread(Receive);
                        receive.IsBackground = true;
                        receive.Start(client);
                    }
                }
                catch (Exception)
                {
                    IP = new IPEndPoint(IPAddress.Any, 9999); //9999: port
                    server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                }
            });
            listen.IsBackground = true;
            listen.Start();

        }

        /// <summary>
        /// Đóng kết nối hiện thời
        /// </summary>
        void Close()
        {
            server.Close();
        }

        /// <summary>
        /// Gửi tin
        /// </summary>
        void Send(Socket client)
        {
            if (client != null && txtMessage.Text != string.Empty)
            {
                client.Send(Serialize(txtMessage.Text));
            }
        }

        /// <summary>
        /// Nhận tin
        /// </summary>
        void Receive(object obj)
        {
            Socket client = obj as Socket;
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string message = (string)Deserialize(data);

                    foreach (Socket item in clientList)
                    {
                        if (item != null && item != client)
                        {
                            item.Send(Serialize(message));

                        }

                    }

                    AddMessage(message);
                }
            }
            catch (Exception ex)
            {
                clientList.Remove(client);
                client.Close();
            }


        }

        /// <summary>
        /// Add message vào khung chat
        /// </summary>
        /// <param name="str"></param>
        void AddMessage(string str)
        {
            lvMessage.Items.Add(new ListViewItem() { Text = str });

        }

        /// <summary>
        /// Phân mảnh
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            return stream.ToArray();
        }

        /// <summary>
        /// Gom mảnh lại
        /// </summary>
        /// <returns></returns>
        object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(stream);
        }


        /// <summary>
        /// Đóng kết nối
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Server_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
        }
    }
}
